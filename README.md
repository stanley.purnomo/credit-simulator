CREDIT SIMULATOR
================

How to operate:
================
- Open this project folder root and open `./credit_simulator` script file from Terminal. If got permission denied, please use this command first: `chmod +x credit_simulator`
- This program will be taking input values from `credit_simulator_file_inputs.txt` file.
- After that, this program will be calculating monthly installment depends on txt input values.
- If gonna load existing calculation, just type `yes` and press Enter button on below the confirmation of load existing calculation. If don't, please type `no` and press Enter button.
- Finally, the program will be showing calculate monthly installment if the API input rules is correct or notice if the API input rules is wrong.

How to use the test unit:
=========================
Just launch this command on Terminal from project folder root.
Compile Unit Test:
`javac -cp ./test-unit-tools/junit-4.12.jar:test-unit-tools/hamcrest-core-1.3.jar:. VehicleUnitTest.java`

Run Unit Test:
`java -cp ./test-unit-tools/junit-4.12.jar:test-unit-tools/hamcrest-core-1.3.jar:. org.junit.runner.JUnitCore VehicleUnitTest`