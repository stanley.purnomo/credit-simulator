public class Vehicle {
  private String type, year, condition; // private = restricted access

  public String getType() {
    return type;
  }

  public void setType(String newType) {
    this.type = newType;
  }

  public String getCondition() {
    return condition;
  }

  public void setCondition(String newCondition) {
    this.condition = newCondition;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String newYear) {
    this.year = newYear;
  }
}