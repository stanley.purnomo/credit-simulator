# we will use openjdk 8 with alpine as it is a very small linux distro
FROM openjdk:8-jre-alpine3.9

# set the startup command to execute the jar
CMD ["java", "-cp", "./lib/json-simple-1.1.1.jar:", "Main"]