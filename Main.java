import java.util.*;
import java.io.*;
import java.net.URL;
import java.time.Year;
import java.nio.charset.Charset;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
public class Main { 
  public static double round(double value, int places) {
    if (places < 0) throw new IllegalArgumentException();

    long factor = (long) Math.pow(10, places);
    value = value * factor;
    long tmp = Math.round(value);
    return (double) tmp / factor;
  }

	public static void main(String[] args) throws Exception {

    Scanner input = new Scanner(System.in);
    FileReader fr = new FileReader("credit_simulator file_inputs.txt");
    BufferedReader reader = new BufferedReader(fr);
    ArrayList<String> li = new ArrayList<>();

    // **** key is declared here in this block of code
    String key = "";
    String line = reader.readLine();

    while (line != null) {
        // System.out.println(line);
        li.add(line);
        key += line;
        line = reader.readLine();
    }

    Vehicle vh = new Vehicle();
    vh.setType(li.get(0));
    vh.setCondition(li.get(1));
    vh.setYear(li.get(2));
    String vehicleType = vh.getType();
    String vehicleCondition = vh.getCondition();
    String vehicleYear = vh.getYear();
    int totalLoan = Integer.parseInt(li.get(3));
    int tenorLoan = Integer.parseInt(li.get(4));
    int downPayment = Integer.parseInt(li.get(5));
    int interestRate = 0;
    int year = Year.now().getValue();
    if(vehicleType.matches("(?i).*mobil.*")){
      interestRate = 8;
    } else {
      interestRate = 9;
    }
    System.out.println("Input Jenis Kendaraan Motor|Mobil (Alphabet, Ignore Cased)): " + vehicleType);
    System.out.println("Input Kendaraan Bekas|Baru. (Alphabet, Ignore Cased): " + vehicleCondition);
    System.out.println("Input Tahun Kendaraan (Numeric, 4 Digit): " + vehicleYear);
    System.out.println("Input Jumlah Pinjaman Total. (Numeric <= 1 miliyar): " + totalLoan);
    System.out.println("Input Tenor Pinjaman 1-6 thn.: " + tenorLoan);
    System.out.println("Input Jumlah DP: Rp. " + String.format("%,.2f", (double)downPayment));
    System.out.println("Suku Bunga Awal: " + interestRate + "%");
    
    if(tenorLoan > 6){
       System.out.println("Tenor pinjaman tidak boleh lebih dari 6 bulan");
    } else if(totalLoan > 1000000000L){
        System.out.println("Total pinjaman harus kurang dari sama dengan 1 M");
    } else if(vehicleType.matches("(?i).*mobil.*") && downPayment < (0.25 * totalLoan) || !vehicleType.matches("(?i).*mobil.*") && downPayment < (0.35 * totalLoan)){
        System.out.println("DP Mobil harus diatas sama dengan 25% dan Motor harus diatas sama dengan 35%");
    } else if(Integer.parseInt(vehicleYear) != year && vehicleCondition.matches("(?i).*baru.*")){
          System.out.println("Tahun mobil/motor harus sama dengan tahun ini jika kondisi mobil/motor baru");
    } else {
      int remainingBalance = 0;
      double interest = 0.0;
      int totalLoanBalance = 0;
      int months = 0;
      int installmentMonthly = 0;
      int installmentYearly = 0;
      int previousTotalLoanBalance = 0;
      double previousInterest = 0;
      int previousInstallmentYearly = 0;
      for(int i = 0; i < tenorLoan; i++){
        if(i==0){
          remainingBalance = totalLoan - downPayment; // pokok pinjaman
          interest = interestRate / 100.0; // suku bunga
          totalLoanBalance = (int) remainingBalance + (int) (remainingBalance * interest);
          months = tenorLoan * 12;
          installmentMonthly = totalLoanBalance / months;
          installmentYearly = installmentMonthly * 12;
          previousTotalLoanBalance = totalLoanBalance;
          previousInterest = interest;
          previousInstallmentYearly = installmentYearly;
          System.out.println("Tahun " + (i+1) + ": Rp. " + String.format("%,.2f", (double)installmentMonthly) + "/bln , Suku Bunga: " + (interest*100) + "%");
        } else {
          remainingBalance = previousTotalLoanBalance - previousInstallmentYearly; // pokok pinjaman
          if((i+1)%2==0){
            interest = previousInterest + (0.1/100.0); // suku bunga
          } else {
            interest = previousInterest + (0.5/100.0); // suku bunga
          }
          totalLoanBalance = (int) remainingBalance + (int) (remainingBalance * interest);
          months = tenorLoan * 12;
          installmentMonthly = totalLoanBalance / (months-(i*12));
          installmentYearly = installmentMonthly * 12;
          previousTotalLoanBalance = totalLoanBalance;
          previousInterest = interest;
          previousInstallmentYearly = installmentYearly;
          System.out.println("Tahun " + (i+1) + ": Rp. " + String.format("%,.2f", (double)installmentMonthly) + "/bln , Suku Bunga: " + round((interest*100), 2) + "%");
        }
      }
      System.out.println("Do you want to load existing calculation?");
      String confirmation = input.nextLine();
      if(confirmation.equals("yes")){
        // URL url = new URL("http://www.mocky.io/v2/5d06e6ae3000005300051d16");
        URL url = new URL("http://www.mocky.io/v2/5d06e6ae3000005300051d16");
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(new InputStreamReader(url.openStream()));
        JSONObject vehicleModel = (JSONObject) json.get("vehicleModel");
        Vehicle vh1 = new Vehicle();
        vh1.setType((String)vehicleModel.get("vehicleType"));
        vh1.setCondition((String)vehicleModel.get("vehicleCondition"));
        vh1.setYear((String)vehicleModel.get("tahunMobil"));
        vehicleType = vh1.getType();
        vehicleCondition = vh1.getCondition();
        vehicleYear = vh1.getYear();
        totalLoan = Integer.parseInt((String)vehicleModel.get("jumlahPinjaman"));
        tenorLoan = Integer.parseInt((String)vehicleModel.get("tenorCicilan"));
        downPayment = Integer.parseInt((String)vehicleModel.get("jumlahDownPayment"));
        interestRate = 0;
      if(vehicleType.matches("(?i).*mobil.*")){
        interestRate = 8;
      } else {
        interestRate = 9;
      }
      System.out.println("Input Jenis Kendaraan Motor|Mobil (Alphabet, Ignore Cased)): " + vehicleType);
      System.out.println("Input Kendaraan Bekas|Baru. (Alphabet, Ignore Cased): " + vehicleCondition);
      System.out.println("Input Tahun Kendaraan (Numeric, 4 Digit): " + vehicleYear);
      System.out.println("Input Jumlah Pinjaman Total. (Numeric <= 1 miliyar): " + totalLoan);
      System.out.println("Input Tenor Pinjaman 1-6 thn.: " + tenorLoan);
      System.out.println("Input Jumlah DP: Rp. " + String.format("%,.2f", (double)downPayment));
      System.out.println("Suku Bunga Awal: " + interestRate + "%");
      
      if(tenorLoan > 6){
        System.out.println("Tenor pinjaman tidak boleh lebih dari 6 bulan");
      } else if(totalLoan > 1000000000L){
          System.out.println("Total pinjaman harus kurang dari sama dengan 1 M");
      } else if(vehicleType.matches("(?i).*mobil.*") && downPayment < (0.25 * totalLoan) || !vehicleType.matches("(?i).*mobil.*") && downPayment < (0.35 * totalLoan)){
          System.out.println("DP Mobil harus diatas sama dengan 25% dan Motor harus diatas sama dengan 35%");
      } else if(Integer.parseInt(vehicleYear) != year && vehicleCondition.matches("(?i).*baru.*")){
          System.out.println("Tahun mobil/motor harus sama dengan tahun ini jika kondisi mobil/motor baru");
      } else {
        remainingBalance = 0;
        interest = 0.0;
        totalLoanBalance = 0;
        months = 0;
        installmentMonthly = 0;
        installmentYearly = 0;
        previousTotalLoanBalance = 0;
        previousInterest = 0;
        previousInstallmentYearly = 0;
        for(int i = 0; i < tenorLoan; i++){
          if(i==0){
            remainingBalance = totalLoan - downPayment; // pokok pinjaman
            interest = interestRate / 100.0; // suku bunga
            totalLoanBalance = (int) remainingBalance + (int) (remainingBalance * interest);
            months = tenorLoan * 12;
            installmentMonthly = totalLoanBalance / months;
            installmentYearly = installmentMonthly * 12;
            previousTotalLoanBalance = totalLoanBalance;
            previousInterest = interest;
            previousInstallmentYearly = installmentYearly;
            System.out.println("Tahun " + (i+1) + ": Rp. " + String.format("%,.2f", (double)installmentMonthly) + "/bln , Suku Bunga: " + (interest*100) + "%");
          } else {
            remainingBalance = previousTotalLoanBalance - previousInstallmentYearly; // pokok pinjaman
            if((i+1)%2==0){
              interest = previousInterest + (0.1/100.0); // suku bunga
            } else {
              interest = previousInterest + (0.5/100.0); // suku bunga
            }
            totalLoanBalance = (int) remainingBalance + (int) (remainingBalance * interest);
            months = tenorLoan * 12;
            installmentMonthly = totalLoanBalance / (months-(i*12));
            installmentYearly = installmentMonthly * 12;
            previousTotalLoanBalance = totalLoanBalance;
            previousInterest = interest;
            previousInstallmentYearly = installmentYearly;
            System.out.println("Tahun " + (i+1) + ": Rp. " + String.format("%,.2f", (double)installmentMonthly) + "/bln , Suku Bunga: " + round((interest*100), 2) + "%");
          }
        }
      }
      }
    }
	}
}